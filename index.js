//make express application
//assign port to that application
import express, { json } from "express";
import firstRouter from "./src/Router/firstRouter.js";

import bike from "./src/Router/bike.js";

import vehicle from "./src/Router/vehicle.js";

import connectToMongoDB from "./src/mongoDbConnection/mongoDbConnection.js";
import studentRouter from "./src/Router/studentRouter.js";
import bookRouter from "./src/Router/bookRouter.js";
import teacherRouter from "./src/Router/teacherRouter.js";
import collegeRouter from "./src/Router/collegeRouter.js";
import classroomRouter from "./src/Router/classroomRouter.js";
import departmentRouter from "./src/Router/departmentRouter.js";
import traineeRouter from "./src/Router/traineeRouter.js";
import testRouter from "./src/Router/testRouter.js";
import webuserRouter from "./src/Router/webuserRouter.js";
import productRouter from "./src/Router/productRouter.js";
import reviewRouter from "./src/Router/reviewRouter.js";
import cors from "cors";
import bcrypt from "bcrypt"
import { port } from "./src/config.js";
import jwt from "jsonwebtoken"
import fileRouter from "./src/Router/fileRouter.js";




let expressApp = express(); //app is created

expressApp.use(json()); 
expressApp.use(cors())    //always place this code at top of the router eg. expressApp.use("/", firstRouter);

// to make static folder
expressApp.use(express.static("./public"))//every file must be place at static folder so that we could get image through link

// let port = 8000;
// let port = process.env.PORT

expressApp.listen(port, () => {
  //port is assigned to our app
  console.log(`Express App is listening at port ${port}`)
  
});



// Application Middleware
// expressApp.use(
//   (req,res,next)=>{
//     console.log("i am application middleware 1")
//     next()
//   },
//   (req,res,next)=>{
//     console.log("i am application middleware 2")
//     next()
//   },
//   (req,res,next)=>{
//     console.log("i am application middleware 3")
//     next()
//   }
//   )


connectToMongoDB()

 


expressApp.use("/", firstRouter);
expressApp.use("/bike", bike);
expressApp.use("/trainee", traineeRouter);
expressApp.use("/vehicle", vehicle);
expressApp.use("/students",studentRouter)
expressApp.use("/books",bookRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/college",collegeRouter)
expressApp.use("/classroom",classroomRouter)
expressApp.use("/department",departmentRouter)
expressApp.use("/test",testRouter)
expressApp.use("/webuser",webuserRouter)
expressApp.use("/products",productRouter)
expressApp.use("/reviews",reviewRouter)
expressApp.use("/files",fileRouter)




//send data from postman
// -->body = req.body
// -->query = req.query
//  -->params = req.params

//url
//localhost:8000/students?name=manish&age=25
//url = route+query
//route = localhost:8000/students (student route parameter)
// route parameter is divided into two parts
//one is static route parameter > must use same name as mention
//another one is dynamic route parameter > can use any name
//query = manish&age=25  =>query parameter
//whatever we pass in query, it always takes values as string


//always place API (which has dynamic route parameter) at last





// //register
// let password = "Password@123"
// let hashPassword = await bcrypt.hash(password, 10)
// console.log(hashPassword)

// //login
// let isValidPassword = await bcrypt.compare("Password@123",hashPassword)
// console.log(isValidPassword)



//generate token
// let infoObject = {
//   id: "12345678"
//   //generally we give id at infoObject
// }

// let secretKey = "dw9"

// let expiryInfo = {
//   expiresIn: "365d"
// }



// let token = jwt.sign(infoObject,secretKey,expiryInfo)
// console.log(token)

// let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQ1Njc4IiwiaWF0IjoxNzAxODMxNjUyLCJleHAiOjE3MzMzNjc2NTJ9.6zGnnYYkxhiMbgBnPYkS7BhkoaB9loYxkSdJz5s_Qfc"

// let infoObject = jwt.verify(token,"dw9")

// console.log(infoObject)

//valid token
//token must be made from the given secretKey
//not expired


