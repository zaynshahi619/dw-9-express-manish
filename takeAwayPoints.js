// CRUD

// create --> post
// read   --> get
// update --> patch
// delete --> delete



//backend
// -->make api
//      -->defining task for each request is called making api



// postman
//    -->hit api
//       -->test api



//if you send data from body always use post and patch method



//schema->model->controller->route->index



//SEARCHING                                                         //aggregation seaching for advance searching
// find({name:'nitan'})
// find({name:"nitan",age:29})
// find({age:27})  //type doesnt matter
// find({age:"27"}) //same as above


//find greater than 25
//find ({age:{$gt:25}})

//greater than and equal to
// find ({age:{$gte:25}})

//less than
//find ({age:{$lt:25}})

//less than equal to
//find ({age:{$lte:25}})

//not equal to
//find({name:{$ne:"nitan"}})


// find age between 15 and 25 and including both
//find({age:{$gte:15,$lte:25}})

//finding those whose name is bikram,bikash
//find({name:{$in:["bikram","bikash"]}})

//selects all 3
// find({$or:[{name:"bikram"},{name:"ram"}]})

//select all 2
// find({$or:[{name:"bikram",age:31},{name:"ram"}]})


// find({$or:[{name:"bikram"},{age:31}]})
// find({$and:[{name:"bikram"},{age:31}]})


//task2
// find age between 26 and 30 using AND

//find those user whose does not include  bikash and bikram




//REGEX SEARCHING  --> Not equal searching

    // let result = await Student.find({name:"manish"})   //exact
    // let result = await Student.find({name:/manish/})  //not exact
    // let result = await Student.find(name:/manish/i)  //not exact
    // let result = await Student.find(name:/^ma/)      //not exact
    // let result = await Student.find(name:/sh$/)      //not exact
         //not exact
    



//SORTING
  

    // let result = await Student.find({}).sort("name")   //ascending
    // let result = await Student.find({}).sort("-name")  //descending with name


    // let result = await Student.find({}).sort("name age")  //first sort with names, if common name then sort with age



    



    //working order --> find,sort,select,skip,limit




    //Interview question
    //power of JS
    // "1"-1= 0      //tries to convert string to number and does operation




    