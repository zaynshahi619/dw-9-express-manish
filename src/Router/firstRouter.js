import { Router } from "express";

let firstRouter = Router()

firstRouter
.route("/")  // while selecting API it sees the route, it doesn't sees url
.post((req,res)=>{
    // console.log("body data",req.body)
    // console.log("query data",req.query)

    res.json("home post")
    // res.json("one response")
    //postman only takes 1 request and 1 response
})







firstRouter
.route("/name")
.post((req,res)=>{
    res.json("name post")
})



firstRouter
// .route("/:country")   //always put dynamic at last
.route("/a/:country/b/:name")
.post((req,res)=>{
    console.log(req.params)
    res.json('hello')
    
})


export default firstRouter