import { Router } from "express";
import { bookCreate, bookDelete, bookRead, bookReadSpecific, bookUpdate } from "../controller/bookController.js";

let bookRouter =Router()

bookRouter
.route("/")
.post(bookCreate)
.get(bookRead)



bookRouter
.route("/:bookId")
.patch(bookUpdate)
.delete(bookDelete)
.get(bookReadSpecific)

export default bookRouter