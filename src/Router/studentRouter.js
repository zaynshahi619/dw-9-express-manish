import { Router } from "express";

import { studentCreate, studentDelete, studentRead, studentReadSpecific, studentUpdate } from "../controller/studentController.js";


let studentRouter = Router()

studentRouter
.route("/")
.post(studentCreate)


//try catch for post
// .post(async(req,res)=>{

//     try {
//         let data=req.body//save data to student
//         let result = Student.create(data)
//         res.json({
//         success:true,
//         message:"post student message successful"
//     })
//     } catch (error) {
//         res.json({
//             success:false,
//             message:error.message
//         })
//     }
    
// })



.get(studentRead)




// .patch((req,res)=>{
//     res.json({
//         success:true,
//         message:"patch message successful"
//     })
// })
// .delete((req,res)=>{
//     res.json({
//         success:true,
//         message:"delete message successful"
//     })
// })


//update 
studentRouter
.route("/:studentId")
.patch(studentUpdate
    
    )
.delete(studentDelete)
//read specific


.get(studentReadSpecific)


export default studentRouter