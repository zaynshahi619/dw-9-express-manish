import { Router } from "express";
import { collegeCreate, collegeDelete, collegeRead, collegeReadSpecific, collegeUpdate } from "../controller/collegeController.js";

let collegeRouter =Router()

collegeRouter
.route("/")
.post(collegeCreate)
.get(collegeRead)



collegeRouter
.route("/:collegeId")
.patch(collegeUpdate)
.delete(collegeDelete)
.get(collegeReadSpecific)

export default collegeRouter