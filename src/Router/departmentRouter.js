import { Router } from "express";
import { departmentCreate, departmentDelete, departmentRead, departmentReadSpecific, departmentUpdate } from "../controller/departmentController.js";

let departmentRouter = Router()

departmentRouter
.route("/")
.post(departmentCreate)
.get(departmentRead)



departmentRouter
.route("/:departmentId")
.patch(departmentUpdate)
.delete(departmentDelete)
.get(departmentReadSpecific)

export default departmentRouter