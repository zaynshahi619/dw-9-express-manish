import { Router } from "express";

import { reviewCreate, reviewDelete, reviewRead, reviewReadSpecific, reviewUpdate } from "../controller/reviewController.js";


let reviewRouter = Router()

reviewRouter
.route("/")
.post(reviewCreate)
.get(reviewRead)


reviewRouter
.route("/:reviewId")
.patch(reviewUpdate)
.delete(reviewDelete)
.get(reviewReadSpecific)


export default reviewRouter