import { Router } from "express";

import { testCreate, testDelete, testRead, testReadSpecific, testUpdate } from "../controller/testController.js";


let testRouter = Router()

testRouter
.route("/")
.post(testCreate)


//try catch for post
// .post(async(req,res)=>{

//     try {
//         let data=req.body//save data to test
//         let result = Test.create(data)
//         res.json({
//         success:true,
//         message:"post test message successful"
//     })
//     } catch (error) {
//         res.json({
//             success:false,
//             message:error.message
//         })
//     }
    
// })



.get(testRead)




// .patch((req,res)=>{
//     res.json({
//         success:true,
//         message:"patch message successful"
//     })
// })
// .delete((req,res)=>{
//     res.json({
//         success:true,
//         message:"delete message successful"
//     })
// })


//update 
testRouter
.route("/:testId")
.patch(testUpdate
    
    )
.delete(testDelete)
//read specific


.get(testReadSpecific)


export default testRouter