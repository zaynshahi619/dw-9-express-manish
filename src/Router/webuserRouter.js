import { Router } from "express";

import { loginWebuser, webuserCreate, webuserDelete, webuserRead, webuserReadSpecific, webuserUpdate } from "../controller/webuserController.js";


let webuserRouter = Router()

webuserRouter
.route("/")
.post(webuserCreate)
.get(webuserRead)





webuserRouter
.route("/login")
.post(loginWebuser)






//dynamic route should always be placed at last
webuserRouter
.route("/:webuserId")
.patch(webuserUpdate)
.delete(webuserDelete)
.get(webuserReadSpecific)


export default webuserRouter