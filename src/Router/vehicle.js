import { Router } from "express";

let vehicle = Router()

vehicle
.route("/")
.post(()=>{
    console.log("vehicle created successfully")
})
.get(()=>{
    console.log("vehicle read successfully")
})
.patch(()=>{
    console.log("vehicle updated successfully")
})
.delete(()=>{
    console.log("vehicle deleted successfully")
})

export default vehicle