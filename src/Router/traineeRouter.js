import { Router } from "express";

import { traineeCreate, traineeDelete, traineeRead, traineeReadSpecific, traineeUpdate } from "../controller/traineeController.js";


let traineeRouter = Router()

traineeRouter
.route("/")
.post(traineeCreate)
.get(traineeRead)


traineeRouter
.route("/:traineeId")
.patch(traineeUpdate)
.delete(traineeDelete)
.get(traineeReadSpecific)


export default traineeRouter