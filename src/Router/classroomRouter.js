import { Router } from "express";
import { classroomCreate, classroomDelete, classroomRead, classroomReadSpecific, classroomUpdate } from "../controller/classroomController.js";

let classroomRouter = Router()

classroomRouter
.route("/")
.post(classroomCreate)
.get(classroomRead)



classroomRouter
.route("/:classroomId")
.patch(classroomUpdate)
.delete(classroomDelete)
.get(classroomReadSpecific)

export default classroomRouter