import { Router } from "express";
import { teacherCreate, teacherDelete, teacherRead, teacherReadSpecific, teacherUpdate } from "../controller/teacherController.js";

let teacherRouter = Router()

teacherRouter
.route("/")
.post(teacherCreate)
.get(teacherRead)


teacherRouter
.route("/:teacherId")
.patch(teacherUpdate)
.delete(teacherDelete)
.get(teacherReadSpecific)


export default teacherRouter