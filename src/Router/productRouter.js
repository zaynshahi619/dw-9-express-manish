import { Router } from "express";

import { productCreate, productDelete, productRead, productReadSpecific, productUpdate } from "../controller/productController.js";


let productRouter = Router()

productRouter
.route("/")
.post(productCreate)
.get(productRead)


productRouter
.route("/:productId")
.patch(productUpdate)
.delete(productDelete) 
.get(productReadSpecific)


export default productRouter