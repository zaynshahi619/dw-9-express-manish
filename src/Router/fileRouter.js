import { Router } from "express";
import upload from "../middleware/upload.js";

let fileRouter = Router()




let uploadMultipleFile = (req,res,next)=>{
    console.log(req.files)  //from middleware upload

    let links = req.files.map((value,index)=>{
        return `http://localhost:8000/${value.filename}`
    }) 

    res.json({
        success:true,
        message:"upload file successfully",
        result:links
    })
}

fileRouter
.route("/")
.post(upload.array("document",5),uploadMultipleFile)

export default fileRouter