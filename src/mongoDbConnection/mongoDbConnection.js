import mongoose from "mongoose"

let connectToMongoDB = async() =>{
    try{
        await mongoose.connect("mongodb://0.0.0.0:27017/dw9")
        console.log("our application is connected to mongodb successfully")
    } catch (error){
        console.log(error.message)
    }
}

export default connectToMongoDB