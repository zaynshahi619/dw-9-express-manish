import { Webuser } from "../schema/studentModel.js";
import bcrypt, { hash } from "bcrypt"
import { sendMail } from "../utils/sendmail.js";



export let webuserCreate = async(req, res) => {

  try {
    let data = req.body; //save data to student

    //hashing password
    let password = data.password
    let hashPassword = await bcrypt.hash(password,10)
    
    data = {
      ...data,   //spread all data
      password: hashPassword  //replace password key with new hashPassword
    }
 
    let result = await Webuser.create(data);

    await sendMail({
      from:'"Unique" <zaynshahi619@gmail.com>',
      to:[req.body.email],
      subject: "Message",
      html: `<p>this is my mail 2</p>`
    })



    res.json({
      success: true,
      message: "Webuser post message successful",
      result:result
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let webuserRead = async (req, res) => {
  try {
    let result = await Webuser.find({});

    res.json({
      success: true,
      message: "Webuser get successful",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let webuserUpdate = async (req, res) => {
  let webuserId = req.params.webuserId;
  let data = req.body;

  try {
    let result = await Webuser.findByIdAndUpdate(webuserId, data);
    res.json({
      success: true,
      message: "webuser updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let webuserDelete = async (req, res) => {
  let webuserId = req.params.webuserId;

  try {
    let result = await Webuser.findByIdAndDelete(webuserId);
    res.json({
      success: true,
      message: "webuser deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let webuserReadSpecific = async (req, res) => {
  let webuserId = req.params.webuserId;

  try {
    let result = await Webuser.findById(webuserId);
    res.json({
      success: true,
      message: "webuser specific read  successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};


// if email exist then check password then compare to the password in database
export let loginWebuser = async(req,res)=>{
    let email = req.body.email
    let password = req.body.password

    try {
      let user = await Webuser.findOne({email:email})
      if(user === null){
        res.json({
          success:false,
          message:"Email or Password does not match"
        })
      }
      else{
        let databasePassword = user.password
        let isValidPassword = await bcrypt.compare(password,databasePassword)

        if(isValidPassword){
          res.json({
            success:true,
            message:"login successfully"
          })
        }
        else{
          res.json({
            success:false,
          message:"Email or Password does not match"
          })
        }
      }
      


    } catch (error) {
      res.json({
        success:false,
        message: "Email or Password does not match"
      })
    }
}

// email, password
// get email and password
// if email exist
// if email exist we check password match
// if match res = success true
//if not false