import { Test } from "../schema/studentModel.js";

export let testCreate = (req, res) => {
  let data = req.body; //save data to test
  let result = Test.create(data);
  res.json({
    success: true,
    message: "post test message successful",
  });
};

export let testRead = async (req, res) => {
  try {
    // let result = await Test.find({});

    // let result = await Test.find({name:'bikram'})
   
    // let result = await Test.find({name:'ram',age:25})

    //finding age greater than 25
    // let result = await Test.find({age:{$gt:25}})
  

    //finding age less than 26
    // let result = await Test.find({age:{$lt:26}})
 

    //finding age between 30 and 35
    // let result = await Test.find({age:{$gt:30,$lt:35}})
    

    //finding those whose name is bikram,bikash
    // let result = await Test.find({name:{$in:["bikram","bikash"]}})
   


    // let result = await Test.find({$or:[{name:"bikram"},{name:"ram"}]})
    

    
    // let result = await Test.find({$and:[{age:{$gte:26}},{age:{$lte:35}}]})

  

    // let result = await Test.find({name:{$nin:["bikram","bikash"]}})






    res.json({
      success: true,
      message: "get message successful",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let testUpdate = async (req, res) => {
  let testId = req.params.testId;
  let data = req.body;

  try {
    let result = await Test.findByIdAndUpdate(testId, data);
    res.json({
      success: true,
      message: "test updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let testDelete = async (req, res) => {
  let testId = req.params.testId;

  try {
    let result = await Test.findByIdAndDelete(testId);
    res.json({
      success: true,
      message: "test deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let testReadSpecific = async (req, res) => {
  let testId = req.params.testId;

  try {
    let result = await Test.findById(testId);
    res.json({
      success: true,
      message: "test read  successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
