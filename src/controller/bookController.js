import { Book } from "../schema/studentModel.js";

export let bookCreate = (req, res) => {
    let data = req.body; //save data to student
    let result = Book.create(data);
    res.json({
      success: true,
      message: "book post message successful",
    });
  };


  export let bookRead = async (req, res) => {
    try {
      let result = await Book.find({});
      res.json({
        success: true,
        message: "book get successful",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let bookUpdate = async (req, res) => {
    let bookId = req.params.bookId;
    let data = req.body;
  
    try {
      let result = await Book.findByIdAndUpdate(bookId, data);
      res.json({
        success: true,
        message: "book updated successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let bookDelete = async (req, res) => {
    let bookId = req.params.bookId;
  
    try {
      let result = await Book.findByIdAndDelete(bookId);
      res.json({
        success: true,
        message: "book deleted successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };


  export let bookReadSpecific = async (req, res) => {
    let bookId = req.params.bookId;
  
    try {
      let result = await Book.findById(bookId);
      res.json({
        success: true,
        message: "book specific read  successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };