// import { College } from "../schema/studentModel.js";

import { College } from "../schema/studentModel.js";


export let collegeCreate = (req, res) => {
    let data = req.body; //save data to student
    let result = College.create(data);

    res.json({
      success: true,
      message: "college post message successful",
    });
  };



  
  export let collegeRead = async (req, res) => {


    let query = req.query
    
    try {
      let result = await College.find(query);
      res.json({
        success: true,
        message: "college get successful",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let collegeUpdate = async (req, res) => {
    let collegeId = req.params.collegeId;
    let data = req.body;
  
    try {
      let result = await College.findByIdAndUpdate(collegeId, data);
      res.json({
        success: true,
        message: "college updated successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let collegeDelete = async (req, res) => {
    let collegeId = req.params.collegeId;
  
    try {
      let result = await College.findByIdAndDelete(collegeId);
      res.json({
        success: true,
        message: "college deleted successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };


  export let collegeReadSpecific = async (req, res) => {
    let collegeId = req.params.collegeId;
  
    try {
      let result = await College.findById(collegeId);
      res.json({
        success: true,
        message: "college specific read  successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };