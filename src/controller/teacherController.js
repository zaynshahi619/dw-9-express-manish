import { Teacher } from "../schema/studentModel.js";


export let teacherCreate = (req, res) => {
    let data = req.body; //save data to student
    let result = Teacher.create(data);
    res.json({
      success: true,
      message: "Teacher post message successful",
    });
  };


  export let teacherRead = async (req, res) => {
    try {
      let result = await Teacher.find({});
      res.json({
        success: true,
        message: "Teacher get successful",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let teacherUpdate = async (req, res) => {
    let teacherId = req.params.teacherId;
    let data = req.body;
  
    try {
      let result = await Teacher.findByIdAndUpdate(teacherId, data);
      res.json({
        success: true,
        message: "teacher updated successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let teacherDelete = async (req, res) => {
    let teacherId = req.params.teacherId;
  
    try {
      let result = await Teacher.findByIdAndDelete(teacherId);
      res.json({
        success: true,
        message: "teacher deleted successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };


  export let teacherReadSpecific = async (req, res) => {
    let teacherId = req.params.teacherId;
  
    try {
      let result = await Teacher.findById(teacherId);
      res.json({
        success: true,
        message: "teacher specific read  successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };