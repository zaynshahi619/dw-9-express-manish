import { Department } from "../schema/studentModel.js";


export let departmentCreate = (req, res) => {
    let data = req.body; //save data to student
    let result = Department.create(data);
    res.json({
      success: true,
      message: "Department post message successful",
    });
  };


  export let departmentRead = async (req, res) => {
    try {
      let result = await Department.find({});
      res.json({
        success: true,
        message: "Department get successful",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let departmentUpdate = async (req, res) => {
    let departmentId = req.params.departmentId;
    let data = req.body;
  
    try {
      let result = await Department.findByIdAndUpdate(departmentId, data);
      res.json({
        success: true,
        message: "department updated successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let departmentDelete = async (req, res) => {
    let departmentId = req.params.departmentId;
  
    try {
      let result = await Department.findByIdAndDelete(departmentId);
      res.json({
        success: true,
        message: "department deleted successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };


  export let departmentReadSpecific = async (req, res) => {
    let departmentId = req.params.departmentId;
  
    try {
      let result = await Department.findById(departmentId);
      res.json({
        success: true,
        message: "department specific read  successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };