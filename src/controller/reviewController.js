import { Review } from "../schema/studentModel.js";

export let reviewCreate = async(req, res) => {
  try {
    let data = req.body; //save data to student
    let result = await Review.create(data);
    res.json({
      success: true,
      message: "Review post message successful",
      result:result
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let reviewRead = async (req, res) => {
  try {                                         //expand     
    let result = await Review.find({}).populate("productId","name price").populate("userId","name email");

    res.json({
      success: true,
      message: "Review get successful",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let reviewUpdate = async (req, res) => {
  let reviewId = req.params.reviewId;
  let data = req.body;

  try {
    let result = await Review.findByIdAndUpdate(reviewId, data);
    res.json({
      success: true,
      message: "review updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let reviewDelete = async (req, res) => {
  let reviewId = req.params.reviewId;

  try {
    let result = await Review.findByIdAndDelete(reviewId);
    res.json({
      success: true,
      message: "review deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let reviewReadSpecific = async (req, res) => {
  let reviewId = req.params.reviewId;

  try {
    let result = await Review.findById(reviewId);
    res.json({
      success: true,
      message: "review specific read  successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
