import { Classroom } from "../schema/studentModel.js";


export let classroomCreate = (req, res) => {
    let data = req.body; //save data to student
    let result = Classroom.create(data);
    res.json({
      success: true,
      message: "Classroom post message successful",
    });
  };


  export let classroomRead = async (req, res) => {
    try {
      let result = await Classroom.find({});
      res.json({
        success: true,
        message: "Classroom get successful",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let classroomUpdate = async (req, res) => {
    let classroomId = req.params.classroomId;
    let data = req.body;
  
    try {
      let result = await Classroom.findByIdAndUpdate(classroomId, data);
      res.json({
        success: true,
        message: "classroom updated successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };

  export let classroomDelete = async (req, res) => {
    let classroomId = req.params.classroomId;
  
    try {
      let result = await Classroom.findByIdAndDelete(classroomId);
      res.json({
        success: true,
        message: "classroom deleted successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };


  export let classroomReadSpecific = async (req, res) => {
    let classroomId = req.params.classroomId;
  
    try {
      let result = await Classroom.findById(classroomId);
      res.json({
        success: true,
        message: "classroom specific read  successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };