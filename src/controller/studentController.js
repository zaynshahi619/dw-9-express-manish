import { Student } from "../schema/studentModel.js";

export let studentCreate = (req, res) => {
  let data = req.body; //save data to student
  let result = Student.create(data);
  res.json({
    success: true,
    message: "post student message successful",
  });
};

export let studentRead = async (req, res) => {
  try {
    let result = await Student.find({});

    // let result = await Student.find({name:'bikram'})
    // let result = await Student.find({name:'bikram',age:31})

    //finding age greater than 25
    // let result = await Student.find({age:{$gt:25}})

    //finding age less than 26
    // let result = await Student.find({age:{$lt:26}})

    //finding age between 30 and 35
    // let result = await Student.find({age:{$gt:30,$lt:35}})

    //finding those whose name is bikram,bikash
    // let result = await Student.find({name:{$in:["bikram","bikash"]}})

    // let result = await Student.find({$or:[{name:"bikram"},{name:"ram"}]})

    // let result = await Student.find({$and:[{age:{$gte:26}},{age:{$lte:35}}]})

    // let result = await Student.find({name:{$nin:["bikram","bikash"]}})

    
    //REGEX SEARCHING --> not exact searching
    // let result = await Student.find({name:"manish"})   //exact
    // let result = await Student.find({name:/manish/})  //not exact
    // let result = await Student.find({name:/manish/i})  //not exact
    // let result = await Student.find({name:/^ma/})      //not exact
    // let result = await Student.find({name:/sh$/})      //not exact
    
    //finding 6 letters word   //can search in chatgpt
    // let result = await Student.find({name:/^.{6}$/})
    
    


    //SORTING

    // let result = await Student.find({}).sort("name")   //ascending
    // let result = await Student.find({}).sort("-name")  //descending with name


    // let result = await Student.find({}).sort("name age")  //first sort with names, if common name then sort with age

    // let result = await Student.find({age:{$gte:30}}).sort("name")
    
    
    // let result = await Student.find({}).select("name -_id")     //find has control over object and select has control over object property



    //Limit & Skip
    // let result = await Student.find({}).limit('3').skip("0")




    







    res.json({
      success: true,
      message: "get message successful",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let studentUpdate = async (req, res) => {
  let studentId = req.params.studentId;
  let data = req.body;

  try {
    let result = await Student.findByIdAndUpdate(studentId, data);
    res.json({
      success: true,
      message: "student updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let studentDelete = async (req, res) => {
  let studentId = req.params.studentId;

  try {
    let result = await Student.findByIdAndDelete(studentId);
    res.json({
      success: true,
      message: "student deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let studentReadSpecific = async (req, res) => {
  let studentId = req.params.studentId;

  try {
    let result = await Student.findById(studentId);
    res.json({
      success: true,
      message: "student read  successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
