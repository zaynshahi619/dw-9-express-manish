import { Trainee } from "../schema/studentModel.js";

export let traineeCreate = (req, res) => {
  let data = req.body; //save data to student
  let result = Trainee.create(data);
  res.json({
    success: true,
    message: "Trainee post message successful",
  });
};

export let traineeRead = async (req, res) => {

  // let query = req.query;                      //{name:nitan, age:29}

   let brake = req.query.brake
   let page = req.query.page
     
  
  try {
    // let result = await Trainee.find({})
    let result = await Trainee.find({}).limit(brake).skip((page-1)*brake) //pagination
   

    res.json({
      success: true,
      message: "Trainee get successful",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let traineeUpdate = async (req, res) => {
  let traineeId = req.params.traineeId;
  let data = req.body;

  try {
    let result = await Trainee.findByIdAndUpdate(traineeId, data);
    res.json({
      success: true,
      message: "trainee updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let traineeDelete = async (req, res) => {
  let traineeId = req.params.traineeId;

  try {
    let result = await Trainee.findByIdAndDelete(traineeId);
    res.json({
      success: true,
      message: "trainee deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let traineeReadSpecific = async (req, res) => {
  let traineeId = req.params.traineeId;

  try {
    let result = await Trainee.findById(traineeId);
    res.json({
      success: true,
      message: "trainee specific read  successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
