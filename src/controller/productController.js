import { Product } from "../schema/studentModel.js";

export let productCreate = async(req, res) => {
  try {
    let data = req.body; //save data to student
    let result = await Product.create(data);
    res.json({
      success: true,
      message: "Product post message successful",
      result:result
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message, 
    });
  }
};

export let productRead = async (req, res) => {
  try {
    let result = await Product.find({});

    res.json({
      success: true,
      message: "Product get successful",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let productUpdate = async (req, res) => {
  let productId = req.params.productId;
  let data = req.body;

  try {
    let result = await Product.findByIdAndUpdate(productId, data);
    res.json({
      success: true,
      message: "product updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let productDelete = async (req, res) => {
  let productId = req.params.productId;

  try {
    let result = await Product.findByIdAndDelete(productId);
    res.json({
      success: true,
      message: "product deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let productReadSpecific = async (req, res) => {
  let productId = req.params.productId;

  try {
    let result = await Product.findById(productId);
    res.json({
      success: true,
      message: "product specific read  successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
