import { Schema } from "mongoose";

let testSchema = Schema({
    name: {
        type:String,
        required:[true,"name field is required"],
    },
    age:{
        type:Number,
        required:[true,"age field is required"],
    },
    isMarried:{
        type: Boolean,
        required:[true,"Married field is required"],
    },
})

export default testSchema