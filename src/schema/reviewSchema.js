import { Schema } from "mongoose";

let reviewSchema = Schema({
    productId:{
        // type: String,
        required:[true,"name field is required"],
        type: Schema.ObjectId,
        ref: "Product"
    },
    userId:{
        // type: String,
        required:[true,"price field is required"],
        type: Schema.ObjectId,
        ref: "Webuser"
    },
    rating:{
        type: Number,
        required:[true,"quantity field is required"]
    },
    description:{
        type: String,
        required:[true,"quantity field is required"]
    }
   

    
})

export default reviewSchema


