import { Schema } from "mongoose";

let departmentSchema = Schema({
    name:{
        type: String,
        required:[true,"name field is required"],
    },
    hod:{
         type: String,
         required:[true,"name field is required"],
   },
   totalMember:{
        type: Number,
        required:[true,"totalMember field is required"]
   }
})

export default departmentSchema