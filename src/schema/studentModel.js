//defining array is called model

import { model } from "mongoose";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import bookSchema from "./bookSchema.js";
import traineeSchema from "./traineeSchema.js";
import collegeSchema from "./collegeSchema.js";
import classroomSchema from "./classroomSchema.js";
import departmentSchema from "./departmentSchema.js";
import testSchema from "./testSchema.js";
import webuserSchema from "./webuserSchema.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";

export let Student = model("Student",studentSchema);
export let Teacher = model("Teacher",teacherSchema)
export let Book = model("Book",bookSchema);
export let College = model("College",collegeSchema);
export let Trainee = model("Trainee",traineeSchema)
export let Classroom = model("Classroom",classroomSchema)
export let Department = model("Department",departmentSchema)
export let Test = model("Test",testSchema)
export let Webuser = model("Webuser",webuserSchema)
export let Product = model("Product",productSchema)
export let Review = model("Review",reviewSchema)




//model name must be singular and first letter capital
//match
