import { Schema } from "mongoose";

let webuserSchema = Schema({
    name:{
        type: String,
        required:[true,"name field is required"],
        // lowercase:true
        // uppercase:true
        trim:true,
        minLength:3,    //incase of string we use minLength and maxLength
        maxLength:10,

        //name fill only contains alphabet and numbers
        validate:(value)=>{
            let isValid = /^[a-zA-Z0-9]*$/.test(value)
            if(!isValid){
                throw new Error("only alphabet and numbers")
            }
        }


    },
    age:{
        type:Number,
        required:[true,"age field is required"],
        min:18,      //in case of Number we use min and max
        max:50,
        validate: (value)=>{
            if(value===35){
                let error = new Error("your age is not allowed")
                throw error
            }
        }

    },
    email:{
        type:String,
        required:[true,"email field is required"],
        unique:true
        //once define unique true we only can remove it from mongodb compass
    },
    password:{
        type:String,
        required:[true,"password field is required"],
    },
    phoneNumber:{
        type:Number,
        required:[true,"phoneNumber field is required"],
        // unique:true
        validate: (value)=>{
            let str = String(value)
            if(str.length!==10){
                throw new Error('phone number must be 10 digit')
            }
        }
    },
    isMarried:{
        type:Boolean,
        required:[true,"isMarried field is required"],
    },
    spouseName:{
        type:String,
        required:[function(){
            if(this.isMarried){
                return true
            }
            else{
                return false
            }
        },
        "spouseName field is required"
    ]
        
    },
    gender:{
        type:String,
        required:[true,"Gender field is required"],
        default: "female"
    },
    dob:{
        type:Date,
        required:[true,"Date field is required"],

    },
    location:{
        country:{type:String},
        exactLocation:{type:String}
    },
    favTeacher:[
        {
            type:String,
            required:[true,"favTeacher field is required"]
        }
    ],
    favSubject:[
        {bookName:{type:String},
         bookAuthor:{type:String}
        }
    ],
    
})

export default webuserSchema


